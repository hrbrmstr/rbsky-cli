Source code from [this blog post](https://rud.is/b/2023/07/07/poor-dudes-janky-bluesky-feed-reader-cli-via-r-python/)

- see note in code regarding how to get `atproto` installed (you will need python, unfortunately)
- you'll need the `crayon` and `lubridate` and `reticulate` R packages installed; the first two ship as one of the far-to-many {tidyverse} dependencies, so you likely have them
- change the shebang (if needed)
- `chmod 755` it
- put it somewhere on your `PATH`
- set up `BSKY_USER` and `BSKY_KEY` envars
- …
- profit?

<img src="rbsky-output.webp" width="50%" alt="Output of rbsky script"/>
